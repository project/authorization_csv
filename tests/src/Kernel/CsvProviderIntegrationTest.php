<?php

declare(strict_types=1);

namespace Drupal\Tests\authorization_csv\Kernel;

use Drupal\authorization\Entity\AuthorizationProfile;
use Drupal\authorization_csv\Plugin\authorization\Provider\CsvProvider;
use Drupal\Core\Form\FormState;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\user\Entity\User;

/**
 * Integration tests for CsvProvider.
 *
 * @group authorization_csv
 */
class CsvProviderIntegrationTest extends EntityKernelTestBase {

  /**
   * The provider.
   *
   * @var \Drupal\authorization_csv\Plugin\authorization\Provider\CsvProvider
   */
  protected $provider;

  /**
   * The profile.
   *
   * @var \Drupal\authorization\Entity\AuthorizationProfile
   */
  protected $profile;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'authorization',
    'authorization_csv',
    'authorization_csv_test',
  ];

  /**
   * Setup of kernel tests.
   */
  public function setUp(): void {
    parent::setUp();

    $this->profile = AuthorizationProfile::create([
      'id' => 'csv_profile',
      'label' => 'CSV Profile',
      'provider' => 'csv_provider',
      'consumer' => 'dummy',
    ]);
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'House',
          'operator' => 'in',
          'values' => 'Gryffindor',
        ],
        [
          'column' => 'Species',
          'operator' => 'in',
          'values' => '',
        ],
        [
          'column' => '',
          'operator' => 'in',
          'values' => 'Human',
        ],
      ],
    ]);
    $this->profile->setProviderMappings([
      ['value' => 'Student'],
      ['value' => 'Professor'],
    ]);

    $this->provider = new CsvProvider(
      [
        'profile' => $this->profile,
      ],
      'csv_provider',
      [],
      $this->container->get('string_translation'),
      $this->container->get('entity_type.manager'),
      $this->container->get('entity_field.manager'),
      $this->container->get('authorization_csv.csv')
    );
  }

  /**
   * Test buildConfigurationForm.
   */
  public function testBuildConfigurationForm() {

    $form = [];
    $form_state = new FormState();
    $form = $this->provider->buildConfigurationForm($form, $form_state);

    $this->assertNotEmpty($form);
    $this->assertCount(6, $form);
  }

  /**
   * Test buildRowForm.
   */
  public function testBuildRowForm() {

    $form = [];
    $form_state = new FormState();
    $form = $this->provider->buildRowForm($form, $form_state);

    $this->assertNotEmpty($form);
    $this->assertCount(1, $form);
    $this->assertArrayHasKey('value', $form);
    $this->assertEquals($form['value']['#default_value'], 'Student');
  }

  /**
   * Test create.
   */
  public function testCreate() {
    $provider = CsvProvider::create($this->container,
      ['profile' => 'csv_profile'],
      'csv_provider',
      []
    );

    $this->assertInstanceOf(CsvProvider::class, $provider);
  }

  /**
   * Test sanitizeProposals.
   */
  public function testSanitizeProposals() {

    $proposals = [
      'Student',
      'The Servant of Lord Voldemort',
      'Wandmaker',
    ];
    $sanitized = $this->provider->sanitizeProposals($proposals);

    $this->assertNotEmpty($sanitized);
    $this->assertCount(3, $sanitized);
    $this->assertEquals($sanitized[0], 'Student');
  }

  /**
   * Test filterProposals.
   */
  public function testFilterProposals() {

    $proposals = [
      'Student',
      'The Servant of Lord Voldemort',
      'Wandmaker',
    ];
    $filtered = $this->provider->filterProposals($proposals, ['value' => 'Student']);

    $this->assertNotEmpty($filtered);
    $this->assertCount(1, $filtered);
    $this->assertEquals($filtered[0], 'Student');
  }

  /**
   * Test getProposals.
   */
  public function testGetProposals() {

    $user = User::create([
      'name' => 'hpotter',
      'mail' => 'hpotter@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertNotEmpty($proposals);
    $this->assertCount(1, $proposals);
    $this->assertEquals($proposals[0], 'Student');
  }

  /**
   * Test getProposals with a user that has no proposals.
   */
  public function testGetProposalsNone() {

    $user = User::create([
      'name' => 'dmalfoy',
      'mail' => 'dmalfoy@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertCount(0, $proposals);
  }

  /**
   * Test getProposals with an invalid file.
   */
  public function testGetProposalsInvalidFile() {
    $config = $this->profile->getProviderConfig();
    $config['csv_location'] = 'invalid.csv';
    $this->profile->setProviderConfig($config);

    $user = User::create([
      'name' => 'hpotter',
      'mail' => 'hpotter@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);
    $this->assertEmpty($proposals);
  }

  /**
   * Test getProposals with an invalid user column.
   */
  public function testGetProposalsInvalidUserColumn() {
    $config = $this->profile->getProviderConfig();
    $config['user_column'] = 'invalid';
    $this->profile->setProviderConfig($config);

    $user = User::create([
      'name' => 'hpotter',
      'mail' => 'hpotter@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);
    $this->assertEmpty($proposals);
  }

  /**
   * Test getProposals. Filter function always returns true.
   */
  public function testGetProposalsFilterFunctionTrue() {
    $config = $this->profile->getProviderConfig();
    $config['filter_function'] = 'authorization_csv_test_filter_function_true';
    $this->profile->setProviderConfig($config);

    $user = User::create([
      'name' => 'hpotter',
      'mail' => 'hpotter@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertNotEmpty($proposals);
    $this->assertCount(9, $proposals);
    $this->assertEquals($proposals[0], 'Student');
  }

  /**
   * Test getProposals. Filter function always returns false.
   */
  public function testGetProposalsFilterFunctionFalse() {
    $config = $this->profile->getProviderConfig();
    $config['filter_function'] = 'authorization_csv_test_filter_function_false';
    $this->profile->setProviderConfig($config);

    $user = User::create([
      'name' => 'hpotter',
      'mail' => 'hpotter@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertCount(0, $proposals);
  }

  /**
   * Test getProposals. With two filters; contains.
   */
  public function testGetProposalsContains2Filters() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Loyalty',
          'operator' => 'contains',
          'values' => 'Dumbledore\'s Army',
        ],
        [
          'column' => 'Loyalty',
          'operator' => 'contains',
          'values' => 'Albus Dumbledore',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'hpotter',
      'mail' => 'hpotter@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertNotEmpty($proposals);
    $this->assertCount(1, $proposals);
    $this->assertEquals($proposals[0], 'Student');
  }

  /**
   * Test getProposalsContains; 2 filters not contains; false.
   */
  public function testGetProposalsNotContains2Filters() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Blood status',
          'operator' => 'not_contains',
          'values' => 'Pure-blood',
        ],
        [
          'column' => 'Blood status',
          'operator' => 'not_contains',
          'values' => 'Half-blood',
        ],
        [
          'column' => 'Blood status',
          'operator' => 'not_contains',
          'values' => '',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'hgranger',
      'mail' => 'hgranger@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertNotEmpty($proposals);
    $this->assertCount(1, $proposals);
    $this->assertEquals($proposals[0], 'Student');
  }

  /**
   * Test getProposalsNotContains; 2 filters; false.
   */
  public function testGetProposalsNotContains2FiltersFalse() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Blood status',
          'operator' => 'not_contains',
          'values' => 'Pure-blood',
        ],
        [
          'column' => 'Blood status',
          'operator' => 'not_contains',
          'values' => 'Half-blood',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'gweasley',
      'mail' => 'gweasley@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertCount(0, $proposals);
  }

  /**
   * Test getProposalsContains; 2 filters; false.
   */
  public function testGetProposalsContains2FiltersFalse() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Loyalty',
          'operator' => 'contains',
          'values' => 'Dumbledore\'s Army',
        ],
        [
          'column' => 'Loyalty',
          'operator' => 'contains',
          'values' => 'Albus Dumbledore',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'hgranger',
      'mail' => 'hgranger@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertCount(0, $proposals);
  }

  /**
   * Test getProposalsContains; army.
   */
  public function testGetProposalsContainsArmy() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Loyalty',
          'operator' => 'contains',
          'values' => implode(PHP_EOL, ['Dumbledore\'s Army', 'Albus Dumbledore']),
        ],
        [
          'column' => 'Gender',
          'operator' => 'contains',
          'values' => '',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'rweasly',
      'mail' => 'rweasly@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertNotEmpty($proposals);
    $this->assertCount(1, $proposals);
    $this->assertEquals($proposals[0], 'Student');
  }

  /**
   * Test getProposalsContains, Dumbledore.
   */
  public function testGetProposalsContainsDumbledore() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Loyalty',
          'operator' => 'contains',
          'values' => implode(PHP_EOL, ['Dumbledore\'s Army', 'Albus Dumbledore']),
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'rhagrid',
      'mail' => 'rhagrid@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertNotEmpty($proposals);
    $this->assertCount(1, $proposals);
    $this->assertEquals($proposals[0], 'Professor');
  }

  /**
   * Test getProposalsContains; false.
   */
  public function testGetProposalsContainsNone() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Loyalty',
          'operator' => 'contains',
          'values' => implode(PHP_EOL, ['Dumbledore\'s Army', 'Albus Dumbledore']),
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'pweasley',
      'mail' => 'pweasley@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertCount(0, $proposals);
  }

  /**
   * Test getProposals; null.
   */
  public function testGetProposalsNull() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Wand',
          'operator' => 'empty',
          'values' => '',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'pweasley',
      'mail' => 'pweasley@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertNotEmpty($proposals);
    $this->assertCount(1, $proposals);
    $this->assertEquals($proposals[0], 'Student');
  }

  /**
   * Test getProposal; null, false.
   */
  public function testGetProposalsNullFalse() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Wand',
          'operator' => 'empty',
          'values' => '',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'hpotter',
      'mail' => 'hpotter@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertCount(0, $proposals);
  }

  /**
   * Test getProposals; not null.
   */
  public function testGetProposalsNotNull() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Wand',
          'operator' => 'not_empty',
          'values' => '',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'pweasley',
      'mail' => 'pweasley@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertCount(0, $proposals);
  }

  /**
   * Test getProposals. Invalid operator.
   */
  public function testGetProposalsInvalidOperator() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'House',
          'operator' => 'invalid',
          'values' => 'Gryffindor',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'hpotter',
      'mail' => 'hpotter@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertCount(0, $proposals);
  }

  /**
   * Test getProposals; not in.
   */
  public function testGetProposalsNotIn() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Species',
          'operator' => 'not_in',
          'values' => 'Human',
        ],
        [
          'column' => 'Blood status',
          'operator' => 'not_in',
          'values' => '',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'rlupin',
      'mail' => 'rlupin@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertNotEmpty($proposals);
    $this->assertCount(1, $proposals);
    $this->assertEquals($proposals[0], 'Professor');
  }

  /**
   * Test getProposals with two filters; not in.
   */
  public function testGetProposalsNotInFalse() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Species',
          'operator' => 'not_in',
          'values' => 'Human',
        ],
        [
          'column' => 'Blood status',
          'operator' => 'not_in',
          'values' => '',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'hpotter',
      'mail' => 'hpotter@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertCount(0, $proposals);
  }

  /**
   * Test getProposals with two filters; in.
   */
  public function testGetProposalsIn2Filters() {
    $this->profile->setProviderConfig([
      'header_offset' => 0,
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'user_column' => 'User',
      'user_property' => 'name',
      'provider_column' => 'Job',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'House',
          'operator' => 'in',
          'values' => implode(PHP_EOL, ['Gryffindor', 'Ravenclaw']),
        ],
        [
          'column' => 'Gender',
          'operator' => 'in',
          'values' => 'Male',
        ],
      ],
    ]);

    $user = User::create([
      'name' => 'ljordan',
      'mail' => 'ljordan@hogwarts.edu',
      'status' => 1,
    ]);
    $user->save();

    $proposals = $this->provider->getProposals($user);

    $this->assertNotEmpty($proposals);
    $this->assertCount(1, $proposals);
    $this->assertEquals($proposals[0], 'Student');
  }

}
