<?php

declare(strict_types=1);

namespace Drupal\Tests\authorization_csv\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the authorization.
 *
 * @group authorization_csv
 */
class AuthorizationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'authorization',
    'authorization_drupal_roles',
    'authorization_csv',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $module_path = \Drupal::service('extension.list.module')->getPath('authorization_csv');

    $this->createRole([], 'student');
    $this->createRole([], 'professor');
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $profile = $entity_type_manager->getStorage('authorization_profile')->create([
      'id' => 'hogwarts',
      'label' => 'Hogwarts School of Witchcraft and Wizardry',
      'provider' => 'csv_provider',
      'provider_config' => [
        'csv_location' => DRUPAL_ROOT . '/' . $module_path . '/tests/csv/test.csv',
        'header_offset' => 0,
        'user_column' => 'User',
        'user_property' => 'name',
        'provider_column' => 'Job',
        'filters' => [
          [
            'column' => 'House',
            'operator' => 'not_empty',
            'values' => '',
          ],
          [
            'column' => 'Gender',
            'operator' => 'in',
            'values' => 'Male',

          ],
        ],
      ],
      'provider_mappings' => [
        [
          'value' => 'Student',
        ],
        [
          'value' => 'Professor',
        ],
      ],
      'consumer' => 'authorization_drupal_roles',
      'consumer_mappings' => [
        [
          'role' => 'student',
        ],
        [
          'role' => 'professor',
        ],
      ],
      'status' => TRUE,
      'synchronization_modes' => [
        'user_logon' => 'user_logon',
      ],
      'synchronization_actions' => [
        'revoke_provider_provisioned' => 'revoke_provider_provisioned',
      ],
    ], 'authorization_profile');

    $profile->save();
  }

  /**
   * Test the authorization.
   */
  public function testAuthorization() {
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $user_storage = $entity_type_manager->getStorage('user');

    $hpotter = $this->createUser([], 'hpotter');
    // Login as Harry Potter.
    $this->drupalLogin($hpotter);
    $hpotter = $user_storage->load($hpotter->id());

    $this->assertTrue($hpotter->hasRole('student'), 'Harry Potter does not have the student role.');
    $this->assertFalse($hpotter->hasRole('professor'), 'Harry Potter has the professor role.');

    $rhagrid = $this->createUser([], 'rhagrid');
    // Login as Rubeus Hagrid.
    $this->drupalLogin($rhagrid);
    $rhagrid = $user_storage->load($rhagrid->id());
    $this->assertTrue($rhagrid->hasRole('professor'), 'Rubeus Hagrid does not have the professor role.');
    $this->assertFalse($rhagrid->hasRole('student'), 'Rubeus Hagrid has the student role.');

    $hgranger = $this->createUser([], 'hgranger');
    // Login as Hermione Granger.
    $this->drupalLogin($hgranger);
    $hgranger = $user_storage->load($hgranger->id());
    $this->assertFalse($hgranger->hasRole('professor'), 'Hermione Granger does not have the professor role.');
    $this->assertFalse($hgranger->hasRole('student'), 'Hermione Granger does not have the student role.');

    $owood = $this->createUser([], 'owood');
    $owood->addRole('professor');
    $owood->save();
    $version = \Drupal::service("extension.list.module")->getExtensionInfo('authorization_drupal_roles')['version'] ?? '0';
    if (version_compare($version, '8.x-1.4', '<')) {
      $owood->authorization_drupal_roles_roles = ['professor'];
      $owood->save();
    }
    else {
      $data = \Drupal::service('authorization_drupal_roles.manager');
      $data->setRoles($owood->id(), 'hogwarts', ['professor']);
    }

    // Login as Oliver Wood.
    $this->drupalLogin($owood);
    $owood = $user_storage->load($owood->id());
    $this->assertFalse($owood->hasRole('professor'), 'Oliver Wood does not have the professor role.');
    $this->assertTrue($owood->hasRole('student'), 'Oliver Wood has the student role.');
  }

}
