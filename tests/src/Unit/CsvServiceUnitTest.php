<?php

declare(strict_types=1);

namespace Drupal\Tests\authorization_csv\Unit;

use Drupal\authorization_csv\Plugin\authorization\Provider\CsvProvider;
use Drupal\authorization_csv\Service\CsvService;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;

/**
 * Tests the CSV service.
 *
 * @group authorization_csv
 */
class CsvServiceUnitTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The CSV service.
   *
   * @var \Drupal\authorization_csv\Service\CsvService
   */
  protected $csvService;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->fileSystem = $this->prophesize(FileSystemInterface::class);
    $this->fileSystem->realpath(__DIR__ . '/../../csv/test.csv')
      ->willReturn(realpath(__DIR__ . '/../../csv/test.csv'));
    $this->logger = $this->prophesize(LoggerInterface::class);

    $this->csvService = new CsvService(
      $this->fileSystem->reveal(),
      $this->logger->reveal()
    );
  }

  /**
   * Test the getProposals method. Invalid CSV location.
   */
  public function testProposalsInvalidCsvLocation() {
    $this->fileSystem->realpath('invalid.csv')->willReturn(FALSE);
    $this->logger->error('The csv file %file does not exist.', [
      '%file' => 'invalid.csv',
    ])->shouldBeCalled();

    $provider_config = [
      'csv_location' => 'invalid.csv',
      'provider_column' => 'column',
      'header_offset' => 0,
    ];
    $callback = function () {
      return [];
    };

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame([], $proposals);
  }

  /**
   * Test the getProposals method. In Operator.
   */
  public function testProposals() {

    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('hpotter');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $provider_config = [
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'provider_column' => 'Job',
      'user_column' => 'User',
      'user_property' => 'name',
      'header_offset' => 0,
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'House',
          'operator' => 'in',
          'values' => 'Gryffindor',
        ],
        [
          'column' => 'Species',
          'operator' => 'in',
          'values' => '',
        ],
        [
          'column' => 'Species',
          'operator' => 'not_in',
          'values' => '',
        ],
        [
          'column' => '',
          'operator' => 'in',
          'values' => 'Human',
        ],
      ],
    ];
    $callback = CsvProvider::csvFilterCallback($user->reveal(), $provider_config);

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame(['Student'], $proposals);
  }

  /**
   * Test the getProposals method. Not In Operator.
   */
  public function testProposalsNotIn() {

    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('hpotter');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $provider_config = [
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'provider_column' => 'Job',
      'user_column' => 'User',
      'user_property' => 'name',
      'header_offset' => 0,
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'House',
          'operator' => 'not_in',
          'values' => 'Gryffindor',
        ],
        [
          'column' => 'Species',
          'operator' => 'in',
          'values' => '',
        ],
        [
          'column' => '',
          'operator' => 'in',
          'values' => 'Human',
        ],
      ],
    ];
    $callback = CsvProvider::csvFilterCallback($user->reveal(), $provider_config);

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame([], $proposals);
  }

  /**
   * Test the getProposals method. Invalid Operator.
   */
  public function testProposalsInvalidOperator() {

    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('hpotter');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $provider_config = [
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'provider_column' => 'Job',
      'user_column' => 'User',
      'user_property' => 'name',
      'header_offset' => 0,
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'House',
          'operator' => 'invalid',
          'values' => 'Gryffindor',
        ],
        [
          'column' => 'Species',
          'operator' => 'in',
          'values' => '',
        ],
        [
          'column' => '',
          'operator' => 'in',
          'values' => 'Human',
        ],
      ],
    ];
    $callback = CsvProvider::csvFilterCallback($user->reveal(), $provider_config);

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame([], $proposals);
  }

  /**
   * Test the getProposals method. Null Operator.
   */
  public function testProposalsNullOperator() {

    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('hpotter');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $provider_config = [
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'provider_column' => 'Job',
      'user_column' => 'User',
      'user_property' => 'name',
      'header_offset' => 0,
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Death',
          'operator' => 'empty',
          'values' => '',
        ],
      ],
    ];
    $callback = CsvProvider::csvFilterCallback($user->reveal(), $provider_config);

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame(['Student'], $proposals);
  }

  /**
   * Test the getProposals method. Not Null Operator.
   */
  public function testProposalsNotNullOperator() {

    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('adumbledore');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $provider_config = [
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'provider_column' => 'Job',
      'user_column' => 'User',
      'user_property' => 'name',
      'header_offset' => 0,
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Death',
          'operator' => 'not_empty',
          'values' => '',
        ],
      ],
    ];
    $callback = CsvProvider::csvFilterCallback($user->reveal(), $provider_config);

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame(['Headmaster'], $proposals);
  }

  /**
   * Test the getProposals method. Not Null Operator.
   */
  public function testProposalsNotNullOperator2() {

    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('sblack');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $provider_config = [
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'provider_column' => 'Gender',
      'user_column' => 'User',
      'user_property' => 'name',
      'header_offset' => 0,
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'Wand',
          'operator' => 'not_empty',
          'values' => '',
        ],
      ],
    ];
    $callback = CsvProvider::csvFilterCallback($user->reveal(), $provider_config);

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame([], $proposals);
  }

  /**
   * Test the getProposals method. Invalid user column.
   */
  public function testGetProposalsInvalidUserColumn() {
    $provider_config = [
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'provider_column' => 'Job',
      'user_column' => 'Invalid',
      'user_property' => 'name',
      'header_offset' => 0,
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'House',
          'operator' => 'in',
          'values' => 'Gryffindor',
        ],
        [
          'column' => 'Species',
          'operator' => 'in',
          'values' => '',
        ],
        [
          'column' => '',
          'operator' => 'in',
          'values' => 'Human',
        ],
      ],
    ];
    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('hpotter');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $callback = CsvProvider::csvFilterCallback($user->reveal(), $provider_config);

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame([], $proposals);
  }

  /**
   * Test the getProposals method. Invalid user property.
   */
  public function testGetProposalsInvalidUserProperty() {
    $provider_config = [
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'provider_column' => 'Job',
      'user_column' => 'User',
      'user_property' => 'invalid',
      'header_offset' => 0,
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'House',
          'operator' => 'in',
          'values' => 'Gryffindor',
        ],
        [
          'column' => 'Species',
          'operator' => 'in',
          'values' => '',
        ],
        [
          'column' => '',
          'operator' => 'in',
          'values' => 'Human',
        ],
      ],
    ];

    $user = $this->prophesize(UserInterface::class);

    $callback = CsvProvider::csvFilterCallback($user->reveal(), $provider_config);

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame([], $proposals);
  }

  /**
   * Test the getProposals method. Invalid offset.
   */
  public function testGetProposalsInvalidOffset() {
    $provider_config = [
      'csv_location' => __DIR__ . '/../../csv/test.csv',
      'provider_column' => 'Job',
      'user_column' => 'User',
      'user_property' => 'name',
      'header_offset' => 1,
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'House',
          'operator' => 'in',
          'values' => 'Gryffindor',
        ],
        [
          'column' => 'Species',
          'operator' => 'in',
          'values' => '',
        ],
        [
          'column' => '',
          'operator' => 'in',
          'values' => 'Human',
        ],
      ],
    ];
    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('hpotter');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $callback = CsvProvider::csvFilterCallback($user->reveal(), $provider_config);

    $proposals = $this->csvService->getProposals($provider_config, $callback);
    $this->assertSame([], $proposals);
  }

}
