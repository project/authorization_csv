<?php

declare(strict_types=1);

namespace Drupal\Tests\authorization_csv\Unit;

use Drupal\authorization\AuthorizationProfileInterface;
use Drupal\authorization_csv\AuthorizationCsvInterface;
use Drupal\authorization_csv\Plugin\authorization\Provider\CsvProvider;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Tests the CSV provider.
 *
 * @group authorization_csv
 */
class CsvProviderUnitTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The authorization CSV service.
   *
   * @var \Drupal\authorization_csv\AuthorizationCsvInterface
   */
  protected $authorizationCsvService;

  /**
   * The profile.
   *
   * @var \Drupal\authorization\AuthorizationProfileInterface
   */
  protected $profile;

  /**
   * The consumer.
   *
   * @var \Drupal\authorization_csv\Plugin\authorization\Provider\CsvProvider
   */
  protected $consumer;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->container = new ContainerBuilder();

    $string_translation = $this->getStringTranslationStub();
    $this->container->set('string_translation', $string_translation);

    $this->entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->container->set('entity_type.manager', $this->entityTypeManager->reveal());

    $this->entityFieldManager = $this->prophesize(EntityFieldManager::class);
    $this->container->set('entity_field.manager', $this->entityFieldManager->reveal());

    $this->authorizationCsvService = $this->prophesize(AuthorizationCsvInterface::class);
    $this->container->set('authorization_csv.csv', $this->authorizationCsvService->reveal());

    \Drupal::setContainer($this->container);

    $this->profile = $this->prophesize(AuthorizationProfileInterface::class);

    $configuration = [
      'profile' => $this->profile->reveal(),
    ];

    $this->consumer = new CsvProvider(
      $configuration,
      'authorization_csv',
      [],
      $string_translation,
      $this->entityTypeManager->reveal(),
      $this->entityFieldManager->reveal(),
      $this->authorizationCsvService->reveal());

  }

  /**
   * Test buildConfigurationForm.
   */
  public function testBuildConfigurationForm() {
    $name = $this->prophesize(FieldStorageDefinitionInterface::class);
    $name->getLabel()->willReturn('Name');
    $this->entityFieldManager->getFieldStorageDefinitions('user')
      ->willReturn([
        'name' => $name->reveal(),
      ])
      ->shouldBeCalled($this->once());

    $this->profile->getProviderConfig()->willReturn([
      'csv_location' => 'public://csv.csv',
      'provider_column' => 'role',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'name',
          'operator' => 'in',
          'values' => 'admin',
        ],
        [
          'column' => '',
          'operator' => 'not_in',
          'values' => 'status',
        ],
        [
          'column' => 'status',
          'operator' => 'in',
          'values' => '',
        ],
      ],

    ])
      ->shouldBeCalled($this->once());

    $form = [];
    $form_state = $this->prophesize(FormStateInterface::class);
    $build = $this->consumer->buildConfigurationForm($form, $form_state->reveal());

    $this->assertIsArray($build);
    $this->assertCount(6, $build);
    $this->assertCount(9, $build['filters']);
  }

  /**
   * Test buildRowForm.
   */
  public function testBuildRowForm() {
    $this->profile->getProviderMappings()
      ->willReturn([
        'column' => 'column-name',
      ])
      ->shouldBeCalled($this->once());

    $form = [];
    $form_state = $this->prophesize(FormStateInterface::class);
    $index = 0;
    $build = $this->consumer->buildRowForm($form, $form_state->reveal(), $index);

    $this->assertIsArray($build);
    $this->assertCount(1, $build);
  }

  /**
   * Test create.
   */
  public function testCreate() {
    $configuration = [
      'profile' => $this->profile->reveal(),
    ];

    $consumer = CsvProvider::create($this->container, $configuration, 'authorization_csv', []);
    $this->assertInstanceOf(CsvProvider::class, $consumer);
  }

  /**
   * Test getProposals.
   */
  public function testSanitizeProposals() {
    $proposals = [
      'admin',
      'editor',
      'author',
    ];

    $build = $this->consumer->sanitizeProposals($proposals);
    $this->assertIsArray($build);
    $this->assertCount(3, $build);
    $this->assertEquals($proposals, $build);
  }

  /**
   * Test csvFilterCallback.
   */
  public function testCsvFilterCallback() {
    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('bob');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $callback = CsvProvider::csvFilterCallback($user->reveal(), [
      'csv_location' => 'csv.csv',
      'provider_column' => 'role',
      'user_column' => 'name',
      'user_property' => 'name',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'role',
          'operator' => 'in',
          'values' => implode(PHP_EOL, ['admin', 'administrator', 'superadmin']),
        ],
        [
          'column' => 'status',
          'operator' => 'in',
          'values' => '1',
        ],
        [
          'column' => '',
          'operator' => 'in',
          'values' => '1',
        ],
        [
          'column' => 'status',
          'operator' => 'in',
          'values' => '',
        ],
        [
          'column' => '',
          'operator' => '',
          'values' => '',
        ],
      ],
    ]);

    $this->assertIsCallable($callback);

    $record_a = [
      'name' => 'bob',
      'role' => 'admin',
      'status' => '1',
    ];
    $result_a = $callback($record_a);
    $this->assertTrue($result_a);

    $record_b = [
      'name' => 'tony',
      'role' => 'admin',
      'status' => '1',
    ];
    $result_b = $callback($record_b);
    $this->assertFalse($result_b);

    $record_c = [
      'name' => 'bob',
      'role' => 'superuser',
      'status' => '0',
    ];
    $result_c = $callback($record_c);
    $this->assertFalse($result_c);

    $record_d = [
      'name' => 'bob',
      'role' => 'editor',
      'status' => '1',
    ];
    $result_d = $callback($record_d);
    $this->assertFalse($result_d);
  }

  /**
   * Test csvFilterCallback. Contains.
   */
  public function testCsvFilterCallbackContains() {
    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('bob');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $callback = CsvProvider::csvFilterCallback($user->reveal(), [
      'csv_location' => 'csv.csv',
      'provider_column' => 'role',
      'user_column' => 'name',
      'user_property' => 'name',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'role',
          'operator' => 'contains',
          'values' => implode(PHP_EOL, ['admin', 'editor']),
        ],
      ],
    ]);

    $this->assertIsCallable($callback);

    $record_a = [
      'name' => 'bob',
      'role' => 'admin',
      'status' => '1',
    ];
    $result_a = $callback($record_a);
    $this->assertTrue($result_a);

    $record_b = [
      'name' => 'tony',
      'role' => 'admin',
      'status' => '1',
    ];
    $result_b = $callback($record_b);
    $this->assertFalse($result_b);

    $record_c = [
      'name' => 'bob',
      'role' => 'superuser',
      'status' => '0',
    ];
    $result_c = $callback($record_c);
    $this->assertFalse($result_c);

    $record_d = [
      'name' => 'bob',
      'role' => 'editor',
      'status' => '1',
    ];
    $result_d = $callback($record_d);
    $this->assertTrue($result_d);

    $record_e = [
      'name' => 'bob',
      'role' => 'administrator',
      'status' => '1',
    ];
    $result_e = $callback($record_e);
    $this->assertTrue($result_e);

    $recordf = [
      'name' => 'bob',
      'role' => 'superadmin',
      'status' => '1',
    ];
    $resultf = $callback($recordf);
    $this->assertTrue($resultf);
  }

  /**
   * Test csvFilterCallback. Not contains.
   */
  public function testCsvFilterCallbackNotContains() {
    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('bob');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $callback = CsvProvider::csvFilterCallback($user->reveal(), [
      'csv_location' => 'csv.csv',
      'provider_column' => 'role',
      'user_column' => 'name',
      'user_property' => 'name',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'role',
          'operator' => 'not_contains',
          'values' => implode(PHP_EOL, ['admin']),
        ],
      ],
    ]);

    $this->assertIsCallable($callback);

    $record_a = [
      'name' => 'bob',
      'role' => 'admin',
      'status' => '1',
    ];
    $result_a = $callback($record_a);
    $this->assertFalse($result_a);

    $record_b = [
      'name' => 'tony',
      'role' => 'admin',
      'status' => '1',
    ];
    $result_b = $callback($record_b);
    $this->assertFalse($result_b);

    $record_c = [
      'name' => 'bob',
      'role' => 'superuser',
      'status' => '0',
    ];
    $result_c = $callback($record_c);
    $this->assertTrue($result_c);

    $record_d = [
      'name' => 'bob',
      'role' => 'editor',
      'status' => '1',
    ];
    $result_d = $callback($record_d);
    $this->assertTrue($result_d);
  }

  /**
   * Test csvFilterCallback. Not contains multiple.
   */
  public function testCsvFilterCallbackNotContainsMultiple() {
    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('bob');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());

    $callback = CsvProvider::csvFilterCallback($user->reveal(), [
      'csv_location' => 'csv.csv',
      'provider_column' => 'role',
      'user_column' => 'name',
      'user_property' => 'name',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'role',
          'operator' => 'not_contains',
          'values' => implode(PHP_EOL, ['i', 'm']),
        ],
      ],
    ]);

    $this->assertIsCallable($callback);

    $record_a = [
      'name' => 'bob',
      'role' => 'admin',
      'status' => '1',
    ];
    $result_a = $callback($record_a);
    $this->assertFalse($result_a);

    $record_b = [
      'name' => 'tony',
      'role' => 'admin',
      'status' => '1',
    ];
    $result_b = $callback($record_b);
    $this->assertFalse($result_b);

    $record_c = [
      'name' => 'bob',
      'role' => 'superuser',
      'status' => '0',
    ];
    $result_c = $callback($record_c);
    $this->assertTrue($result_c);

    $record_d = [
      'name' => 'bob',
      'role' => 'editor',
      'status' => '1',
    ];
    $result_d = $callback($record_d);
    $this->assertTrue($result_d);
  }

  /**
   * Test filterProposals. True.
   */
  public function testFilterProposals() {
    $proposed = [
      'admin',
      'author',
    ];

    $provider_mapping = [
      'value' => 'admin',
    ];

    $build = $this->consumer->filterProposals($proposed, $provider_mapping);
    $this->assertIsArray($build);
    $this->assertCount(1, $build);
    $this->assertEquals(['admin'], $build);
  }

  /**
   * Test filterProposals. False.
   */
  public function testFilterProposalsFalse() {
    $proposed = [
      'admin',
      'author',
    ];

    $provider_mapping = [
      'value' => 'editor',
    ];

    $build = $this->consumer->filterProposals($proposed, $provider_mapping);
    $this->assertIsArray($build);
    $this->assertCount(0, $build);
    $this->assertEquals([], $build);
  }

  /**
   * Test getProposals.
   */
  public function testGetProposals() {
    $field = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $field->expects($this->any())
      ->method('__get')
      ->with('value')
      ->willReturn('bob');
    $user = $this->prophesize(UserInterface::class);
    $user->get('name')
      ->willReturn($field)
      ->shouldBeCalled($this->once());
    $provider_config = [
      'csv_location' => 'public://csv.csv',
      'provider_column' => 'role',
      'user_column' => 'name',
      'user_property' => 'name',
      'filter_function' => NULL,
      'filters' => [
        [
          'column' => 'role',
          'operator' => 'in',
          'values' => 'admin',
        ],
        [
          'column' => '',
          'operator' => 'in',
          'values' => 'status',
        ],
        [
          'column' => 'status',
          'operator' => 'in',
          'values' => '',
        ],
      ],
    ];
    $this->profile->getProviderConfig()
      ->willReturn($provider_config)
      ->shouldBeCalled($this->once());

    $this->authorizationCsvService->getProposals($provider_config, Argument::any())
      ->willReturn(['admin'])
      ->shouldBeCalled($this->once());

    $build = $this->consumer->getProposals($user->reveal(), $provider_config);
    $this->assertIsArray($build);
    $this->assertCount(1, $build);
    $this->assertEquals(['admin'], $build);
  }

}
