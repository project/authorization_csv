<?php

declare(strict_types=1);

namespace Drupal\authorization_csv\Service;

use Drupal\authorization_csv\AuthorizationCsvInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Utility\Error;
use League\Csv\Reader;
use Psr\Log\LoggerInterface;

/**
 * CSV Service.
 *
 * Parse the file and return the proposals.
 */
class CsvService implements AuthorizationCsvInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Csv Service.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(FileSystemInterface $file_system, LoggerInterface $logger) {
    $this->fileSystem = $file_system;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getProposals(array $provider_config, \Closure $callback): array {
    $proposals = [];

    $csv_location = $provider_config['csv_location'];
    $provider_column = $provider_config['provider_column'];
    $offset = $provider_config['header_offset'] ?? 0;

    $real_path = $this->fileSystem->realpath($csv_location);
    if (!$real_path) {
      $this->logger->error('The csv file %file does not exist.', [
        '%file' => $csv_location,
      ]);
      return $proposals;
    }
    try {
      $reader = Reader::createFromPath($real_path, 'r');
      $reader->setHeaderOffset($offset);
      $results = $reader->filter($callback);

      foreach ($results->getRecords() as $record) {
        $proposals[] = $record[$provider_column];
      }
    }
    catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    $proposals = array_unique($proposals);

    return $proposals;
  }

}
