<?php

declare(strict_types=1);

namespace Drupal\authorization_csv\Plugin\authorization\Provider;

use Drupal\authorization\Provider\ProviderPluginBase;
use Drupal\authorization_csv\AuthorizationCsvInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\BeforeCommand;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a CSV provider.
 *
 * @AuthorizationProvider(
 *   id = "csv_provider",
 *   label = @Translation("CSV File")
 * )
 */
class CsvProvider extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $handlers = [];

  /**
   * {@inheritdoc}
   */
  protected $syncOnLogonSupported = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $revocationSupported = TRUE;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity Field Manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * CSV Service.
   *
   * @var \Drupal\authorization_csv\AuthorizationCsvInterface
   */
  protected $csv;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param array $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   Translation.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   Entity field manager.
   * @param \Drupal\authorization_csv\AuthorizationCsvInterface $csv
   *   CSV service.
   */
  final public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    TranslationInterface $translation,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManager $entity_field_manager,
    AuthorizationCsvInterface $csv,
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setStringTranslation($translation);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->csv = $csv;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('authorization_csv.csv')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\authorization\AuthorizationProfileInterface $profile */
    $profile = $this->configuration['profile'];

    $provider_config = $profile->getProviderConfig();
    // $form_state->set('provider_filters', $provider_config['filters'] ?? []);
    $user_field_options = [];
    $user_fields = $this->entityFieldManager
      ->getFieldStorageDefinitions('user');
    foreach ($user_fields as $field_name => $field_instance) {
      $user_field_options[$field_name] = $field_instance->getLabel();
    }

    $form = parent::buildConfigurationForm($form, $form_state);

    $form['csv_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File location'),
      '#description' => $this->t('Location of CSV file'),
      '#required' => TRUE,
      '#default_value' => $provider_config['csv_location'] ?? NULL,
    ];
    $form['header_offset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Header offset'),
      '#description' => $this->t('Row to offset in the  CSV file to reach the header'),
      '#default_value' => $provider_config['header_offset'] ?? 0,
    ];
    $form['user_column'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User column'),
      '#description' => $this->t('The field in the CSV file that contains the user property'),
      '#required' => TRUE,
      '#default_value' => $provider_config['user_column'] ?? NULL,
    ];
    $form['user_property'] = [
      '#type' => 'select',
      '#title' => $this->t('User property'),
      '#description' => $this->t('The property in the user entity to match the user column'),
      '#required' => TRUE,
      '#options' => $user_field_options,
      '#default_value' => $provider_config['user_property'] ?? NULL,
    ];
    $form['provider_column'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Provider column'),
      '#description' => $this->t('The field in the CSV file that contains the provider name'),
      '#required' => TRUE,
      '#default_value' => $provider_config['provider_column'] ?? NULL,
    ];
    $form['filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Filters'),
      '#tree' => TRUE,
      '#weight' => 20,
      '#group' => 'configuration',
    ];

    $filters = $provider_config['filters'] ?? [];
    $filter_count = NULL;
    if (is_null($filter_count)) {
      $filter_count = count($filters);
      $form_state->set('provider_filter_count', $filter_count);
    }
    $filter_form = self::buildFilters($filters, $filter_count);
    array_push($form['filters'], ...$filter_form);

    $form['filters']['add_filter'] = [
      '#type' => 'submit',
      '#value' => t('Add filter'),
      '#submit' => ['\Drupal\authorization_csv\Plugin\authorization\Provider\CsvProvider::addFilter'],
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * Build filters.
   *
   * @param array $filters
   *   The filters.
   * @param int $count
   *   The count.
   *
   * @return array
   *   The form elements.
   */
  protected static function buildFilters(array $filters, int $count) {
    $filter = [];
    $index = 0;
    while ($index < $count) {

      $filter[$index] = [
        '#type' => 'fieldset',
        '#title' => t('Filter'),
        '#title_display' => 'invisible',
      ];
      $filter[$index]['column'] = [
        '#type' => 'textfield',
        '#title' => t('Column'),
        '#description' => t('The column in the CSV file that will be used to filter'),
        '#default_value' => $filters[$index]['column'] ?? NULL,
      ];
      $filter[$index]['operator'] = [
        '#type' => 'select',
        '#title' => t('Operator'),
        '#options' => [
          'in' => t('In'),
          'not_in' => t('Not in'),
          'empty' => t('Empty'),
          'not_empty' => t('Not empty'),
          'contains' => t('Contains'),
          'not_contains' => t('Not contains'),
        ],
        '#default_value' => $filters[$index]['operator'] ?? 'in',
      ];

      $filter[$index]['values'] = [
        '#type' => 'textarea',
        '#title' => t('Values'),
        '#description' => t('One value per line'),
        '#default_value' => $filters[$index]['values'] ?? NULL,
      ];

      $index += 1;

    }

    return $filter;
  }

  /**
   * Functionality for our ajax callback.
   *
   * @param array $form
   *   The form being passed in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state, passed by reference so we can modify.
   */
  public static function addFilter(array &$form, FormStateInterface $form_state): void {
    $form_state->set('provider_filter_count', ($form_state->get('provider_filter_count') + 1));
    $form['configuration']['#default_tab'] = 'filters';
    $form_state->setRebuild();
  }

  /**
   * Ajax Callback for the form.
   *
   * @param array $form
   *   The form being passed in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form element we are changing via ajax
   */
  public static function filterAjaxCallback(array &$form, FormStateInterface $form_state) {
    $return = [];
    foreach ($form['provider_config']['filters'] as $key => $filter) {
      if (is_int($key)) {
        $return[$key] = $filter;
      }
    }
    $return[] = self::buildFilters([], 1);
    $response = new AjaxResponse();
    $response->addCommand(new BeforeCommand('#edit-provider-config-filters-add-filter', array_pop($return)));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRowForm(array $form, FormStateInterface $form_state, $index = 0): array {
    $row = [];
    /** @var \Drupal\authorization\AuthorizationProfileInterface $profile */
    $profile = $this->configuration['profile'];
    $mappings = $profile->getProviderMappings();
    $row['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Element'),
      '#description' => $this->t('The value in the CSV file that will be used to match the consumer.'),
      '#maxlength' => 254,
      '#default_value' => $mappings[$index]['value'] ?? NULL,
    ];

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function getProposals(UserInterface $user): array {
    /** @var \Drupal\authorization\AuthorizationProfileInterface $profile */
    $profile = $this->configuration['profile'];
    $provider_config = $profile->getProviderConfig();
    $filter_function = $provider_config['filter_function'] ?? '';
    if (function_exists($filter_function)) {
      $callback = $filter_function($user, $provider_config);
    }
    else {
      $callback = self::csvFilterCallback($user, $provider_config);
    }

    $proposals = $this->csv->getProposals($provider_config, $callback);

    return $proposals;
  }

  /**
   * Default filter callback.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param array $provider_config
   *   The provider configuration.
   *
   * @return \Closure
   *   The filter function.
   */
  public static function csvFilterCallback(UserInterface $user, $provider_config) {
    $user_column = $provider_config['user_column'];
    $filters = $provider_config['filters'];
    $user_property = $provider_config['user_property'];
    $property = $user->get($user_property)?->value;

    return (function (array $record) use ($user_column, $filters, $property): bool {
      if (empty($record[$user_column])) {
        return FALSE;
      }
      if (($record[$user_column] != $property)) {
        return FALSE;
      }
      $result = TRUE;
      foreach ($filters as $filter) {
        if (empty($filter['column'])) {
          continue;
        }

        $column = $record[$filter['column']];
        $column = strtolower($column);

        $values = $filter['values'];
        $values = explode(PHP_EOL, $values);
        $values = array_map('strtolower', $values);
        $values = array_filter($values);
        switch ($filter['operator']) {
          case 'in':
            if (empty($values)) {
              break;
            }
            if (!in_array($column, $values)) {
              $result = FALSE;
            }
            break;

          case 'not_in':
            if (empty($values)) {
              break;
            }
            if (in_array($column, $values)) {
              $result = FALSE;
            }
            break;

          case 'empty':
            if (!empty($column)) {
              $result = FALSE;
            }
            break;

          case 'not_empty':
            if (empty($column)) {
              $result = FALSE;
            }
            break;

          case 'contains':
            if (empty($values)) {
              break;
            }
            $fn = function ($value) use ($column) {
              return mb_strpos($column, $value) !== FALSE;
            };
            $contains = array_map($fn, $values);
            if (!in_array(TRUE, $contains)) {
              $result = FALSE;
            }
            break;

          case 'not_contains':
            if (empty($values)) {
              break;
            }
            $fn = function ($value) use ($column) {
              return mb_strpos($column, $value) !== FALSE;
            };
            $contains = array_map($fn, $values);
            if (!in_array(FALSE, $contains)) {
              $result = FALSE;
            }
            break;

          default:
            $result = FALSE;
            break;
        }

      }

      return $result;
    });
  }

  /**
   * {@inheritdoc}
   */
  public function filterProposals(array $proposed, array $provider_mapping): array {

    $filtered = [];
    $provider = $provider_mapping['value'] ?? '';
    foreach ($proposed as $proposal) {
      if (mb_strtolower($proposal) == mb_strtolower($provider)) {
        $filtered[] = $proposal;
      }
    }

    return $filtered;
  }

  /**
   * {@inheritdoc}
   */
  public function sanitizeProposals(array $proposals): array {
    return $proposals;
  }

}
