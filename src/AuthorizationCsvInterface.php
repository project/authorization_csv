<?php

declare(strict_types=1);

namespace Drupal\authorization_csv;

/**
 * Interface for the CSV authorization service.
 */
interface AuthorizationCsvInterface {

  /**
   * Get proposals.
   *
   * @param array $provider_config
   *   The provider configuration.
   * @param \Closure $callback
   *   The callback used to filter the CSV.
   *
   * @return array
   *   The proposals.
   */
  public function getProposals(array $provider_config, \Closure $callback): array;

}
